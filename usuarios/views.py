import json
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.views.generic import CreateView
from django.urls import reverse_lazy

from usuarios.forms import RegistroForm
from usuarios.serializers import registro_usuario

class RegistroUsuario(CreateView):
	model = User
	template_name = "formularios/registrar.html"
	form_class = RegistroForm
	success_url = reverse_lazy('home')


class UserAPI(APIView):
	def post(self, request):
		serializer = registro_usuario(data = request.data) # Inicializa objeto form pasando el contenido (request)
		if serializer.is_valid(): # valida formulario --> CSRF
			user = serializer.save()
			return Response(serializer.data, status = status.HTTP_201_CREATED)
		else:
			return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)	