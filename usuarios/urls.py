from django.conf.urls import url

from usuarios.views import RegistroUsuario, UserAPI

app_name = 'usuarios'

urlpatterns = [
	url(r'^registrar', RegistroUsuario.as_view(), name="registrar"),
	url(r'^api', UserAPI.as_view(), name="api"),

]
