from django.contrib import admin
from django.conf import settings
from django.urls import path, include, re_path
from django.conf.urls import url
from django.conf.urls.static import static
from exp4 import views
from Proyecto.views import home, logout_view

urlpatterns = [
    path('admin/', admin.site.urls),
	path('app/registro_mascotas', views.registro_mascotas, name='registro_mascotas'),
    path('app/listar_mascotas', views.listar_mascotas, name='listar_mascotas'),
    path('app/detalle/<int:id>/', views.detalle_mascotas, name='detalle_mascotas'),
    path('app/detalle/mascota/<int:id>/', views.editar_mascotas, name='editar_mascotas'),
    path('app/eliminar/<int:pk>/delete/', views.EliminarForm.as_view(), name='eliminar-mascotas'),
    path('', home, name='home'),
    url(r'^app/logout/$', logout_view, name='logout_view'),
    path('exp4/', include('exp4.urls', namespace='exp4')),
    path('usuarios/', include('usuarios.urls', namespace='usuarios')),
    url(r'^auth/', include('rest_framework_social_oauth2.urls')),
    url(r'^auth/', include('social_django.urls', namespace='social')),
]

if settings.DEBUG:

    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
