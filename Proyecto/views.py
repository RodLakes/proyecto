from django.template import RequestContext
from django.shortcuts import render, redirect
from Proyecto.forms import LoginForm
from django.contrib.auth import authenticate, login, logout

from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.contrib.auth.decorators import login_required



 
def login_form(request):
    message= None
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username=request.POST.get('username')
            password=request.POST.get('password')

            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request,user)
                    return HttpResponseRedirect(reverse('home'))
                else:
                    return HttpResponse("Cuenta Inactiva.")
            else:
                message ="Nombre Usuario y/o Contraseña incorrecta"

    else:
        form = LoginForm()
    return render(request,'formularios/login.html', {'message=':message ,'form': form})



def home(request):
    return render(request,'inicio.html')

@login_required
def logout_view(request):
    logout(request)
    return render(request,'inicio.html')


