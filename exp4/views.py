from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, redirect	
from django.template import RequestContext
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.views.generic import DeleteView,FormView
from django.urls import reverse_lazy

from exp4.models import Gato, Socio
from exp4.form import RegistroForm, actualizaMascota

@login_required
def registro_mascotas(request):
	if request.method =='POST': # si peticion es POST
		form = RegistroForm(request.POST, request.FILES) # Inicializa objeto form pasando el contenido (request)
		if form.is_valid(): # valida formulario --> CSRF
			mascota = Gato(
							nombre_gato=form.cleaned_data['nombre_gato'],
							genero=form.cleaned_data['genero'],
							edad=form.cleaned_data['edad'],
							tipo=form.cleaned_data['tipo'],
							imagen=form.cleaned_data['imagen']
						   )
			mascota.save()
			return redirect('registro_mascotas')# carga pagina creala urls
	else:
		form = RegistroForm()
	return render(request,'formularios/registro_mascotas.html', {'form': form})

@login_required
def listar_mascotas(request):
	mascotas = Gato.objects.all()
	return render(request,'list/listar_mascotas.html', {'mascotas': mascotas})

@login_required
def detalle_mascotas(request, id):
	mascota = get_object_or_404(Gato, pk=id)
	return render(request,'list/detalle_mascotas.html', {'mascota': mascota})

@login_required
def editar_mascotas(request, id):
	mascota = get_object_or_404(Gato, pk=id)
	if request.method =='POST': # si peticion es POST
		form = actualizaMascota(request.POST, instance=mascota) # Inicializa objeto form pasando el contenido (request)
		if form.is_valid(): # valida formulario --> CSRF
			form.save()
			return redirect('detalle_mascotas', id)# carga pagina creala urls
	else:
		form = actualizaMascota(instance=mascota)
	return render(request,'formularios/editar_mascotas.html', {'form': form})


class EliminarForm(DeleteView):
	model = Gato
	template_name = 'exp4/gato_confirm_delete.html'
	success_url = reverse_lazy('listar_mascotas')


