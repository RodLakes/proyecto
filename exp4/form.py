from django import forms
from django.forms import ModelForm, ModelChoiceField
from exp4.models import Gato, Socio, Adopcion, Tipo

GENERO = (('M', 'Macho',), ('H', 'Hembra',))

class RegistroForm(ModelForm):
	class Meta:
		model = Gato
		fields = ['nombre_gato','genero','edad','tipo','imagen']


class actualizaMascota(ModelForm):
	class Meta:
		model = Gato
		fields = ['nombre_gato', 'genero', 'edad', 'tipo','imagen']





