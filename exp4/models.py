from django.db import models



GENERO_CHOICES = (
    ('M', 'Macho'),
    ('H', 'Hembra'),
)

class Tipo(models.Model):
	descripcion = models.TextField(verbose_name='descripcion')

	def __str__(self):
		return (self.descripcion)


class Gato(models.Model): #Tabla Gato
	nombre_gato = models.TextField()
	genero = models.CharField(max_length=1, choices=GENERO_CHOICES, verbose_name='genero')
	edad = models.PositiveSmallIntegerField(default=0, verbose_name='edad')
	fecha_registro= models.DateTimeField(auto_now=True)
	tipo= models.ForeignKey(Tipo, on_delete=models.PROTECT)
	imagen = models.ImageField(upload_to='imagenes/rescatados/') # Requiere libreria Pillow

	def __unicode__(self):
		return str(self.imagen)


class Socio(models.Model):
	rut = models.TextField()
	nombres = models.TextField()
	apellido_mat = models.TextField()
	apellido_pat = models.TextField()
	direccion = models.TextField()
	telefono = models.TextField()
	email = models.TextField()
	edad = models.PositiveSmallIntegerField(default=0, verbose_name='edad')

class Adopcion(models.Model):
	gato = models.ForeignKey(Gato, on_delete=models.PROTECT)
	socio = models.ForeignKey(Socio, on_delete=models.PROTECT)
	fecha_adopcion = models.DateTimeField(auto_now=True)

