from django.conf.urls import url
from Proyecto.views import login_form
# SET THE NAMESPACE!
app_name = 'exp4'
# Be careful setting the name to just /login use userlogin instead!
urlpatterns=[

    url(r'^app/login/$',login_form, name='login_form'),
]