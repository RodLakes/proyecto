from django.contrib import admin
from exp4.models import Gato, Socio, Adopcion, Tipo


# Registra los modelos (exp4/models.py) en el panel de administracion.
admin.site.register(Gato)
admin.site.register(Socio)
admin.site.register(Adopcion)
admin.site.register(Tipo)

# Register your models here.
