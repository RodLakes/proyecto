from django import template
from exp4.models import Gato, Socio

register = template.Library()

@register.inclusion_tag('list/carousel.html')
def carousel():
	return {
		'carousel':Gato.objects.all()

	}